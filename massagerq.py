""" This script will try to get information regarding the massage which are free. In the future, it is planned to include the possibility to book massage for the user. Oh! by the way only works on http://app.saudeativars.com.br """
import sys
import requests
import json
import time
import datetime

# Global settings
username = '' # add your username
password = '' # add your password

def do_login():
    r = requests.post('http://app.saudeativars.com.br/scripts/login.php', data={ 'username': username, 'password': password})
    if r.text == 'Username or Password is invalid':
        sys.exit(r.text)
    return  r.cookies

def get_sessions(cookie):
    sessions_with_bad_symbols = requests.get('http://app.saudeativars.com.br/app/schedule/model/sessions.php', cookies=cookie)
    if not sessions_with_bad_symbols.text:
        return []
    sessions_json_string = sessions_with_bad_symbols.text.replace('\n','').replace('\t','')
    data = json.loads(sessions_json_string)
    return data['ScheduleCollection']

def get_valid_times():
    with open("valid-times.txt") as vt:
        valid_times = vt.readlines()
    return [x.strip() for x in valid_times]

def user_wants(session,valid_times):    
    weekday = datetime.date(*map(int, str(session['date']).split('.'))).strftime("%A")
    starttime = str(session['starttime'])
    return ((weekday + " - " + starttime) in valid_times)    

def book_massage_using(cookie, session):
    r = requests.post('http://app.saudeativars.com.br/scripts/schedule_save.php', cookies=cookie ,data={ 'date': session['date'], 'starttime': session['starttime'], 'endtime': session['endtime']})
    return r.status_code == 200

print ("Current date & time " + time.strftime("%c"))
count = 0
cookie = do_login()
valid_times = get_valid_times()
while(count < 150):
    sessions = get_sessions(cookie)
    print (time.strftime("%c") + " - Attempt #" + str(count+1) + " - checking " + str(len(sessions)) + " sessions...")
    for session in sessions:
        if user_wants(session,valid_times):
            print( str(session['date']) + " - " + str(session['starttime']) + ": " + str(session['endtime']) )
            if session['booked'] == 'X':
                print('already booked')
            else:
                print('FREE!')
                if book_massage_using(cookie, session):
                    print('Booked successfully: ' + str(session))
                    sys.exit()
    print('No free session found, trying again...')
    sys.stdout.flush()
    count += 1
