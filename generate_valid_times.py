with open("default-valid-times.txt",'w') as vt:
    for weekday in ['Tuesday', 'Wednesday', 'Thursday', 'Friday']:
        for hour in ['14','15','16','17']:
            for minute in ['00','10','20','30','40','50']:
                vt.write(weekday + " - " + hour + ":" + minute + "\n")
