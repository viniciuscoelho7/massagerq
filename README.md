# Massage Request

## How to use it?

- Download it!
- Change `username` & `password` variables in the file
- Run with virtualenv:
```
$ cd massagerq
$ pip install -r requirements
$ python massagerq.py
```
- You can also use crontab: edit you crontab file with `crontab -e` and add a rule such as `59 13 * * 1 python3.5 [path/to/massagerq.py] >> [path/to/log_file]`.

- You can edit `valid-times.txt` to only evaluate sessions that fit in your calendar.

- If you want to regenerate the default `valid-times.txt`, execute `python3.5 generate_valid_times.py`, it will generate a `default-valid-times.txt` for you.

- Test the script in your terminal in root directory. If doesn't work, copy `valid-times.txt` to root and test again.
